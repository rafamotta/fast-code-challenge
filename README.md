# Fast code challenge

Also available online here: http://bit.ly/fast-code-challenge

## Getting Started

Create an .env.local file with this content:

```bash
NEXT_PUBLIC_OMDB_API_KEY=c32a291e
```

Then, install all dependencies:

```bash
yarn install
```

Then, start the server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Requirements

- [x] Search for a list of movies
- [x] Render a list of matching movie thumbnails
- [x] Select multiple movies from their searches
- [x] Confirm the list of movies they have selected

## Questions

1. What were the most difficult tasks?
   The API response + the 4 hours limit.

API: Wasn't very intuitive. For example, the response of a search without any results returns a response with the same shape of the request with error, instead of a regular response with an empty array of movies. So I had to do some manipulations to achieve something more natural. Apart from this, some movies didn't have posters, and the posters were most of the time in different sizes, so I also had to deal with this to have a good UI.

Time: In terms of time, of course, wasn't that much, so I took some time to find the right packages. That's the reason that I chose Material UI (mobile-first) + React Query. This saved a lot of time.

2. Did you learn anything new while completing this assignment?

Yes. This was the first time that I used react-query. Such a powerful library and made the code cleaner and shorter.

3. What did you not have time to add? What work took the up majority of your time?

Write + Infinite scrolling (when we have a lot of results). I believe what consumed the majority of the time was using react-query, as it was my first time - but still, was worth it.

4. How could the application be improved?

- Write unit tests + Infinite Scrolling (as already mentioned)
- Improve accessibility
- Make the initial screen (when you don't have any search ) better, instead of just one generic text. Probably I would do something similar to Google on the mobile (a big search on the middle of the screen, and when you press on it, it hides this big search and focus the search on the navbar)
- Loading spinner (to indicate the data is being fetched)
- Better Empty States - right now I am just using a regular alerts, which isn't the best component for displaying this type of information
- After making a booking, show a Snackbar component, instead of a native alert
- Loading indicator to the poster, as some of them might take some time to load
