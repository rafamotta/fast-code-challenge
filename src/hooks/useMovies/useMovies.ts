import axios from 'axios';
import { useQuery } from 'react-query';
import {
  ApiResponseError,
  ApiResponseSuccess,
  ApiResponseTransformed,
} from './types';

const fetchMovies = async (search?: string, page?: string) => {
  const { data } = await axios({
    url: 'https://www.omdbapi.com',
    params: {
      apiKey: process.env.NEXT_PUBLIC_OMDB_API_KEY,
      s: search,
      page,
    },
  });

  return data;
};

const useMovies = (query: { page?: string; search?: string }) =>
  useQuery<ApiResponseSuccess, ApiResponseError, ApiResponseTransformed>(
    ['movies', query.search, query.page],
    () => fetchMovies(query.search, query.page),
    {
      enabled: !!query.search,
      keepPreviousData: true,
      retry: false,
      select: (response) => ({
        search: response.Search
          ? response.Search?.map((movie) => ({
              imdbID: movie.imdbID,
              posterSrc: movie.Poster === 'N/A' ? undefined : movie.Poster,
              title: movie.Title,
              type: movie.Type,
              year: movie.Year,
            }))
          : [],
        totalResults: response.totalResults ? Number(response.totalResults) : 0,
      }),
    },
  );

export default useMovies;
