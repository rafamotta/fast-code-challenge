import { Movie } from '@/types';

export type ApiResponseSuccess = {
  Response: string;
  totalResults?: string;
  Search?: [
    {
      imdbID: string;
      Poster: string;
      Title: string;
      Type: string;
      Year: string;
    },
  ];
};

export type ApiResponseError = {
  message: string;
};

export type ApiResponseTransformed = { search: Movie[]; totalResults: number };
