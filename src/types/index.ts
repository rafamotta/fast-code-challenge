export type Movie = {
  imdbID: string;
  posterSrc?: string;
  title: string;
  type: string;
  year: string;
};
