import React, { useState } from 'react';
import useDebounce from 'react-use/lib/useDebounce';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

export type NavbarSearchProps = {
  initialInputValue?: string;
  onInputChange: (nextValue: string) => void;
  renderRightSection: () => React.ReactNode;
};

const NavbarSearch = ({
  initialInputValue = '',
  onInputChange,
  renderRightSection,
}: NavbarSearchProps) => {
  const classes = useStyles();
  const [value, setValue] = useState(initialInputValue);
  useDebounce(() => onInputChange(value.trim()), 300, [value]);

  const handleInputChanged = (event: React.ChangeEvent<HTMLInputElement>) =>
    setValue(event.target.value);

  return (
    <AppBar color="inherit" elevation={0} position="fixed">
      <Container maxWidth="xl">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            Movie store
          </Typography>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              autoCapitalize="off"
              autoCorrect="off"
              autoFocus
              classes={{ root: classes.inputRoot, input: classes.inputInput }}
              inputProps={{ 'aria-label': 'search' }}
              onChange={handleInputChanged}
              placeholder="Search"
              value={value}
            />
          </div>
          <Box ml={1}>{renderRightSection()}</Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.08),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    alignItems: 'center',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    padding: theme.spacing(0, 2),
    pointerEvents: 'none',
    position: 'absolute',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '20ch',
      '&:focus': {
        width: '32ch',
      },
    },
  },
}));

export default NavbarSearch;
