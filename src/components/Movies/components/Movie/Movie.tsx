import React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import MoviePoster from './components/Poster';

export type MovieProps = {
  onClick: () => void;
  posterSrc?: string;
  selected?: boolean;
  testID?: string;
  title: string;
};

const Movie = ({ title, onClick, selected, posterSrc, testID }: MovieProps) => (
  <Box data-test-id={testID} mb={1}>
    <Box mb={1.5}>
      <MoviePoster src={posterSrc} alt={`${title} movie poster`} />
    </Box>
    <Box mb={2}>
      <Typography align="center" noWrap variant="subtitle1">
        {title}
      </Typography>
    </Box>
    <Button
      color="secondary"
      fullWidth
      onClick={onClick}
      size="small"
      variant={selected ? 'contained' : 'outlined'}
    >
      {selected ? 'Added to cart' : 'Add to cart'}
    </Button>
  </Box>
);

export default Movie;
