import React from 'react';
import Box from '@material-ui/core/Box';
import PanoramaIcon from '@material-ui/icons/Panorama';
import { makeStyles } from '@material-ui/core/styles';

export type PosterProps = {
  alt: string;
  src?: string;
};

const Poster = ({ alt, src }: PosterProps) => {
  const classes = useStyles();

  return src ? (
    <img alt={alt} className={classes.img} src={src} />
  ) : (
    <Box
      alignItems="center"
      bgcolor="text.secondary"
      borderRadius={8}
      display="flex"
      height="310px"
      justifyContent="center"
      width="100%"
    >
      <PanoramaIcon />
    </Box>
  );
};

const useStyles = makeStyles((theme) => ({
  img: {
    borderRadius: theme.shape.borderRadius,
    height: '310px',
    objectFit: 'cover',
    width: '100%',
  },
}));

export default Poster;
