import React from 'react';
import xor from 'lodash/xor';
import find from 'lodash/find';
import Alert from '@material-ui/lab/Alert';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { Movie } from '@/types';
import MovieItem from './components/Movie';

export type MoviesProps = {
  movies: Movie[];
  onSelect: (selectedMovies: Movie[]) => void;
  selectedMovies: Movie[];
  totalResults: number;
};

const Movies = ({
  movies,
  onSelect,
  selectedMovies,
  totalResults,
}: MoviesProps) => {
  const handleClick = (selectedMovie: Movie) => () =>
    onSelect(xor(selectedMovies, [selectedMovie]));

  if (totalResults === 0) {
    return (
      <Box mb={2}>
        <Alert severity="info" variant="filled">
          No results found
        </Alert>
      </Box>
    );
  }

  return (
    <>
      <Box mb={2}>
        <Alert severity="info" variant="filled">
          {`Showing ${movies.length} of ${totalResults}`}
        </Alert>
      </Box>
      <Grid container spacing={3}>
        {movies.map((movie) => (
          <Grid item key={movie.imdbID} lg={2} md={3} sm={4} xs={6}>
            <MovieItem
              onClick={handleClick(movie)}
              posterSrc={movie.posterSrc}
              selected={!!find(selectedMovies, movie)}
              testID={`movie-item-${movie.imdbID}`}
              title={`${movie.title} (${movie.year})`}
            />
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default Movies;
