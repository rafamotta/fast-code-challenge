import React from 'react';
import xor from 'lodash/xor';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Container from '@material-ui/core/Container';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Movie } from '@/types';

export type CartDialogProps = {
  movies: Movie[];
  onChange: (selectedMovies: Movie[]) => void;
  onClose: () => void;
  open: boolean;
};

const CartDialog = ({ movies, onChange, onClose, open }: CartDialogProps) => {
  const classes = useStyles();

  const handleRemoveIconClick = (movieToRemove: Movie) => () => {
    const nextMovies = xor(movies, [movieToRemove]);
    onChange(nextMovies);

    if (nextMovies.length === 0) {
      onClose();
    }
  };

  const handleSubmitClick = () => {
    // Do something cool here...
    alert('Good choices my friend. Have fun');
    onChange([]);
    onClose();
  };

  return (
    <Dialog fullScreen open={open} onClose={onClose}>
      <AppBar className={classes.headerAppBar} color="secondary">
        <Toolbar>
          <IconButton
            aria-label="close"
            color="inherit"
            edge="start"
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            My cart
          </Typography>
          <Button autoFocus color="inherit" onClick={onClose}>
            Close
          </Button>
        </Toolbar>
      </AppBar>
      {movies.length === 0 && (
        <Container maxWidth="xl">
          <Box mt={2}>
            <Alert severity="info" variant="filled">
              No movies added yet
            </Alert>
          </Box>
        </Container>
      )}
      <List dense>
        {movies.map((movie) => (
          <ListItem key={movie.imdbID} dense button>
            <ListItemAvatar>
              <Avatar src={movie.posterSrc}>{movie.title}</Avatar>
            </ListItemAvatar>
            <ListItemText primary={`${movie.title} (${movie.year})`} />
            <ListItemSecondaryAction onClick={handleRemoveIconClick(movie)}>
              <IconButton edge="end" aria-label="delete">
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
      <AppBar
        className={classes.footerAppBar}
        color="secondary"
        position="fixed"
      >
        <Toolbar>
          <Box
            alignItems="center"
            display="flex"
            flex={1}
            justifyContent="center"
          >
            <Button
              autoFocus
              color="inherit"
              disabled={movies.length === 0}
              fullWidth
              onClick={handleSubmitClick}
            >
              Close order
            </Button>
          </Box>
        </Toolbar>
      </AppBar>
    </Dialog>
  );
};

const useStyles = makeStyles((theme) => ({
  headerAppBar: {
    position: 'relative',
  },
  footerAppBar: {
    top: 'auto',
    bottom: 0,
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

export default CartDialog;
