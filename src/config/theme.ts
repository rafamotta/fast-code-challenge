import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  shape: {
    borderRadius: 8,
  },
  palette: {
    text: {
      secondary: '#dcdcdc',
    },
    primary: {
      main: '#556cd6',
    },
    secondary: {
      main: '#1DA854',
    },
    background: {
      default: '#f0f2f5',
    },
  },
});

export default theme;
