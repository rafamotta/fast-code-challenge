import React, { useState } from 'react';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Badge from '@material-ui/core/Badge';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import Movies from '@/components/Movies';
import Navbar from '@/components/NavbarSearch';
import CartDialog from '@/components/CartDialog';
import useMovies from '@/hooks/useMovies';
import { Movie } from '@/types';

const Home = () => {
  const [selectedMovies, setSelectedMovies] = useState<Movie[]>([]);
  const [cartOpened, setCartOpened] = useState<boolean>(false);
  const { isReady, query, push } = useRouter();
  const { data, error } = useMovies({
    page: query.page as string | undefined,
    search: query.search as string | undefined,
  });

  const handleSearchInputChanged = (search: string) => {
    const options = { shallow: true, scroll: false };
    push({ query: { search } }, undefined, options);
  };

  if (!isReady) return null;

  return (
    <>
      <Navbar
        initialInputValue={query.search as string | undefined}
        onInputChange={handleSearchInputChanged}
        renderRightSection={() => (
          <>
            <IconButton
              aria-label={`Show ${selectedMovies.length} items in my cart`}
              color="inherit"
              onClick={() => setCartOpened(true)}
            >
              <Badge badgeContent={selectedMovies.length} color="secondary">
                <ShoppingBasket />
              </Badge>
            </IconButton>
            <CartDialog
              movies={selectedMovies}
              onChange={setSelectedMovies}
              onClose={() => setCartOpened(false)}
              open={cartOpened}
            />
          </>
        )}
      />
      <Container maxWidth="xl">
        <Box mt={9}>
          {query.search ? (
            <>
              {data && (
                <Movies
                  movies={data.search}
                  onSelect={setSelectedMovies}
                  selectedMovies={selectedMovies}
                  totalResults={data.totalResults}
                />
              )}
              {error && <Alert severity="error">{error.message}</Alert>}
            </>
          ) : (
            <Box
              alignItems="center"
              display="flex"
              flexDirection="column"
              justifyContent="center"
              mt={12}
            >
              <Typography variant="h6" align="center">
                Start typing something...
              </Typography>
            </Box>
          )}
        </Box>
      </Container>
    </>
  );
};

export default Home;
